﻿using DotNetCoreWebApiCiAndCdCourse.Models;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace DotNetCoreWebApiCiAndCdCourse.Controllers
{
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly ILogger<StudentController> logger;

        public StudentController(ILogger<StudentController> _logger)
        {
            logger = _logger;
        }

        [HttpGet]
        [Route("api/student/list/get")]
        public IActionResult GetStudentList()
        {
            ResultModels result = new ResultModels();
            
            try
            {
                var studentList = new StudentModels().GetStudentList();

                result.isSuccess = true;
                result.data = studentList;

                logger.LogInformation("GetStudentListSuccess");
                return StatusCode((int)HttpStatusCode.OK, result);
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($"InternalServerError: {ex.ToString}");
                return StatusCode((int)HttpStatusCode.InternalServerError, result);
            }
        }
    }
}

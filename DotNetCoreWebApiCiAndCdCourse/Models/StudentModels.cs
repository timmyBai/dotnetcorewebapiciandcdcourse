﻿using DotNetCoreWebApiCiAndCdCourse.VM;

namespace DotNetCoreWebApiCiAndCdCourse.Models
{
    public class StudentModels
    {
        public List<StudentVM> GetStudentList()
        {
            List<StudentVM> list = new List<StudentVM>();

            char[] lastName = new char[]
            {
                '蔡', '林', '趙', '王', '張',
                '李', '劉', '陳', '賴', '宋',
                '郭', '藍', '白', '黃', '蘇',
                '吳', '徐', '高', '鄭', '官'
            };

            for(int i = 0; i < 10; i++)
            {
                Random randomName = new Random();
                int randomNameInt = randomName.Next(0, lastName.Length - 1);
                
                Random randomAge = new Random();
                int randomAgeInt = randomAge.Next(0, 100);

                Random randomGender = new Random();
                int randomGenderInt = randomGender.Next(0, 2);

                list.Add(new StudentVM()
                {
                    studentId = i,
                    studentName = lastName[randomNameInt] + "測試",
                    age = randomAgeInt,
                    gender = randomGenderInt == 1 ? '男' : '女'
                });
            }

            return list;
        }
    }
}

﻿namespace DotNetCoreWebApiCiAndCdCourse.VM
{
    public class StudentVM
    {
        public int studentId { get; set; } = 0;
        public string studentName { get; set; } = "";
        public int age { get; set; } = 0;
        public char gender { get; set; } = '男';
    }
}

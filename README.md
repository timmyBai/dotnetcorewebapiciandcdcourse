# DotNetCoreWebApiCiAndCdCourse

<div>
    <a href="https://dotnet.microsoft.com/en-us/download/dotnet" target="_blank">
        <img src="https://img.shields.io/badge/C%23-6.0.0-blue">
    </a>
    <a href="https://dotnet.microsoft.com/en-us/download/dotnet" target="_blank">
        <img src="https://img.shields.io/badge/.NetCore-6.0.0-blue">
    </a>
    <a href="https://dotnet.microsoft.com/en-us/download/dotnet" target="_blank">
        <img src="https://img.shields.io/badge/WebApi-6.0.0-blue">
    </a>
</div>

DotNetCoreWebApiCiAndCdCourse 是一個建構基本 DotNetCoreWebApiCiAndCdCourse 架構後端解決方案，他基於 C#、.Net Core 6、Web Api 實現，他使用最新後端技術，作為 Gitlab CI/CD 教學使用。

## 功能

```tex
- 學生
    - 取得學生清單
```

## 測試

```tex
dotnet test DotNetCoreWebApiCiAndCdCourse.Tests
```

## 開發

```git
# 克隆項目
git clone https://gitlab.com/timmyBai/dotnetcorewebapiciandcdcourse.git
```

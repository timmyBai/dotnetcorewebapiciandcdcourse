﻿using DotNetCoreWebApiCiAndCdCourse.Tests.Models;
using NUnit.Framework.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace DotNetCoreWebApiCiAndCdCourse.Tests.Controllers
{
    public class StudentController: ApiController
    {
        public ResultModels GetStudentList()
        {
            ResultModels result = new ResultModels();

            try
            {
                var studentList = new StudentModels().GetStudentList();

                result.isSuccess = true;
                result.data = studentList;

                return result;
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                return result;
            }
        }
    }
}

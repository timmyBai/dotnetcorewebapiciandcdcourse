﻿using DotNetCoreWebApiCiAndCdCourse.Tests.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreWebApiCiAndCdCourse.Tests.Tests.Unit
{
    [TestClass]
    public class StudentControllerTests
    {
        [Test]
        [TestMethod]
        public void TestGetStudentList()
        {
            var result = new StudentController().GetStudentList();

            Assert.IsTrue(result.isSuccess == true);
            Assert.IsTrue(result.data != null);
            Assert.IsTrue(result.message == "");
        }

        [Test]
        [TestMethod]
        public void TestGetStudentListNotFail()
        {
            var result = new StudentController().GetStudentList();

            Assert.IsTrue(result.isSuccess != false);
            Assert.IsTrue(result.message != "InternalServerError");
        }
    }
}

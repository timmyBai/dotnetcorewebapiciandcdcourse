﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetCoreWebApiCiAndCdCourse.Tests.VM
{
    public class StudentVM
    {
        public int studentId { get; set; } = 0;
        public string studentName { get; set; } = "";
        public int age { get; set; } = 0;
        public char gender { get; set; } = '男';
    }
}
